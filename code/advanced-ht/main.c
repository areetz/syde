#include <stdlib.h>
#include <stdio.h>
#include "hashtable.h"

int main(void) {
    struct ht *my_ht = ht_create();
    ht_put(my_ht, "Test", 1309);
    ht_put(my_ht, "Rest", -1309);
    ht_put(my_ht, "Power", 8999);
    ht_put(my_ht, "Power", 9001); // update 8999 to 9001

    printf("%d\n", ht_get(my_ht, "Test"));   // => 1309
    printf("%d\n", ht_get(my_ht, "Rest"));   // => -1309
    printf("%d\n", ht_get(my_ht, "Power"));  // => 9001 
    printf("%d\n", ht_get(my_ht, "Powder")); // => INT_MIN

    ht_destroy(my_ht);
    return EXIT_SUCCESS;
}