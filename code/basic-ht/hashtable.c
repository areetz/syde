#include "hashtable.h"
#include <assert.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>

// === struct kvp ============================================================

// A key--value pair
struct kvp {
    char *key;
    int value;
};

// kvp_create(key, value) returns a new key--value pair of <*key, value>.
static struct kvp *kvp_create(const char *key, const int value) {
    assert(key);
    struct kvp *kvp = malloc(sizeof(struct kvp));
    kvp->key = malloc(sizeof(char) * (strlen(key) + 1));
    strcpy(kvp->key, key);
    kvp->value = value;
    return kvp;
}

// kvp_destroy(ht) releases all resources used by *kvp.
static void kvp_destroy(struct kvp *kvp) {
    assert(kvp);
    free(kvp->key);
    free(kvp);
}

// === hash functions ========================================================

static int hash_func(const char *key) {
    assert(key);
    return strlen(key); // there are definitely better hash algorithms available
}

// === struct ht =============================================================

struct ht {
    int len_max;
    struct kvp **data;
};

struct ht *ht_create(void) {
    struct ht *ht = malloc(sizeof(struct ht));
    ht->len_max = 8;                                      // arbitrary, better to pass in as parameter from client
    ht->data = calloc(sizeof(struct kvp *), ht->len_max); // calloc because we use comparison with NULL to detect empty cells
    return ht;
}

void ht_destroy(struct ht *ht) {
    assert(ht);
    for (struct kvp **kvp = ht->data; kvp < ht->data + ht->len_max; ++kvp) {
        if (*kvp != NULL) {
            kvp_destroy(*kvp);
        }
    }
    free(ht->data);
    free(ht);
}

void ht_put(struct ht *ht, const char *key, const int value) {
    assert(ht);
    assert(key);
    int index = hash_func(key) %  ht->len_max;           // calculate index
    if (ht->data[index] == NULL) {                       // insert new kvp
        ht->data[index] = kvp_create(key, value);
    } else if (strcmp(ht->data[index]->key, key) == 0) { // update existing kvp
        ht->data[index]->value = value;
    }
}

int ht_get(const struct ht *ht, const char *key) {
    assert(ht);
    assert(key);
    int index = hash_func(key) %  ht->len_max;
    if (ht->data[index] != NULL && strcmp(ht->data[index]->key, key) == 0) { // same key
        return ht->data[index]->value;
    } else {                                                                 // empty cell or different key
        return INT_MIN;
    }
}