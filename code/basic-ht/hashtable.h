#include <stdbool.h>

// A hashtable
struct ht;

// ht_create() returns a new, empty hash table.
// requires: allocates heap memory [client must call ht_destroy]
struct ht *ht_create(void);

// ht_destroy(ht) releases all resources used by the hash table *ht.
// effects: *ht becomes invalid
// time:     O(n)
void ht_destroy(struct ht *ht);

// ht_put(ht, key, value) inserts the key--value pair of <*key, value> into
//   the hash table *ht.
void ht_put(struct ht *ht, const char *key, const int value);

// ht_get(ht, key) returns the value if the key--value pair with *key from
//   the hash table *ht.
int ht_get(const struct ht *ht, const char *key);